﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace Laba_2_NowYouSeeSharp_2
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public string Path { get; set; } = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + @"\";
        public Uri uri = new Uri("https://bdu.fstec.ru/files/documents/thrlist.xlsx");
        public static string FaileName { get; set; }
        public int NumberOfAttributes { get; } = 15; // Кол-во атрибутов на странице
        public int Page { get; set; } = 1;
        public static bool Rduction { get; set; } = false;
        public static bool CheckUpdate { get; set; } = false;

        public ThreatDatabase[] mainDatabases;
        public ThreatDatabase[] newDatabases;
        public ThreatDatabase_id_Name[] threatDatabase_Id_Names;
        public List<DatabaseOldNew> databaseOldNew;
        public WebClient client = new WebClient();
        public MainWindow()
        {
            InitializeComponent();
            BtnClose.Visibility = Visibility.Hidden;
            TbNumberList.Text = $"с {Page} до {NumberOfAttributes * Page}";
            FaileName = uri.Segments.Last();
            if (!File.Exists(Path + FaileName))
            {
                MessageBoxResult result = System.Windows.MessageBox.Show("Файл не найден, скачать с интернета?", "Файл не найден", MessageBoxButton.YesNo, MessageBoxImage.Question);

                if (result == MessageBoxResult.Yes)//Если нажал нет добавить кнопку скачать
                {
                    if (!string.IsNullOrEmpty(uri.OriginalString))
                    {
                        client.DownloadFile(uri.OriginalString, FaileName);
                    }
                }
                mainDatabases = ExcelParser.ReadExcel(Path + FaileName);
                ThreatDatabase[] listDatabases = new ThreatDatabase[NumberOfAttributes];
                int k = 0;
                for (int i = (Page - 1) * NumberOfAttributes; i < NumberOfAttributes * Page; i++)
                {
                    listDatabases[k] = mainDatabases[i];
                    k++;
                }
                DtThreat.ItemsSource = listDatabases;
            }
            else
            {
                mainDatabases = ExcelParser.ReadExcel(Path + FaileName);
                ThreatDatabase[] listDatabases = new ThreatDatabase[NumberOfAttributes];
                int k = 0;
                for (int i = (Page - 1) * NumberOfAttributes; i < NumberOfAttributes * Page; i++)
                {
                    listDatabases[k] = mainDatabases[i];
                    k++;
                }
                DtThreat.ItemsSource = listDatabases;
            }
        }
        private void DtThreat_AutoGenerateColumns(object sendr, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (Rduction == false)
            {
                switch (e.PropertyName)
                {
                    case "IdenexYBI":
                        e.Column.Header = "Идентификатор угрозы";
                        break;
                    case "NameYBI":
                        e.Column.Header = "Наименование угрозы";
                        break;
                    case "Description":
                        e.Column.Header = "Описание угрозы";
                        break;
                    case "SourceThreats":
                        e.Column.Header = "Источник угрозы";
                        break;
                    case "ObjectAction":
                        e.Column.Header = "Объект воздействия угрозы";
                        break;
                    case "BreachPrivacy":
                        e.Column.Header = "Нарушение конфиденциальности";
                        break;
                    case "ViolationIntegrity":
                        e.Column.Header = "Нарушение целостности";
                        break;
                    case "AccessViolation":
                        e.Column.Header = "Нарушение доступности";
                        break;
                    default:
                        break;
                }
            }
            else
            {
                switch (e.PropertyName)
                {
                    case "IdenexYBI":
                        e.Column.Header = "УБИ";
                        break;
                    case "NameYBI":
                        e.Column.Header = "Наименование угрозы";
                        break;
                    default:
                        break;
                }
            }
        }
        private void BtnPevious_Click(object sender, RoutedEventArgs e)
        {
            if (Rduction == false)
            {
                if (Page == 1)
                {
                    return;
                }
                Page--;
                if (Page != 1)
                {
                    TbNumberList.Text = $"с {(Page - 1) * NumberOfAttributes} до {NumberOfAttributes * Page}";
                }
                else
                {
                    TbNumberList.Text = $"с {Page} до {NumberOfAttributes * Page}";
                }
                ThreatDatabase[] listDatabases = new ThreatDatabase[NumberOfAttributes];
                int k = 0;
                for (int i = (Page - 1) * NumberOfAttributes; i < NumberOfAttributes * Page; i++)
                {
                    listDatabases[k] = mainDatabases[i];
                    k++;
                }
                DtThreat.ItemsSource = listDatabases;
                DtThreat.Items.Refresh();
            }
            else
            {
                if (Page == 1)
                {
                    return;
                }
                Page--;
                if (Page != 1)
                {
                    TbNumberList.Text = $"с {(Page - 1) * NumberOfAttributes} до {NumberOfAttributes * Page}";
                }
                else
                {
                    TbNumberList.Text = $"с {Page} до {NumberOfAttributes * Page}";
                }
                threatDatabase_Id_Names = new ThreatDatabase_id_Name[NumberOfAttributes];
                int k = 0;
                for (int i = (Page - 1) * NumberOfAttributes; i < NumberOfAttributes * Page; i++)
                {
                    threatDatabase_Id_Names[k] = new ThreatDatabase_id_Name { IdenexYBI = mainDatabases[i].IdenexYBI, NameYBI = mainDatabases[i].NameYBI };
                    k++;
                }
                DtThreat.ItemsSource = threatDatabase_Id_Names;
                DtThreat.Items.Refresh();
            }
        }

        private void BtnNext_Click(object sender, RoutedEventArgs e)
        {
            if (Rduction == false)
            {
                if ((Page) * NumberOfAttributes > Convert.ToInt32(mainDatabases.Last().IdenexYBI))
                {
                    return;
                }
                if ((Page + 1) * NumberOfAttributes < Convert.ToInt32(mainDatabases.Last().IdenexYBI))
                {
                    Page++;
                    TbNumberList.Text = $"с {(Page - 1) * NumberOfAttributes} до {NumberOfAttributes * Page}";
                    ThreatDatabase[] listDatabases = new ThreatDatabase[NumberOfAttributes];
                    int k = 0;
                    for (int i = (Page - 1) * NumberOfAttributes; i < NumberOfAttributes * Page; i++)
                    {
                        listDatabases[k] = mainDatabases[i];
                        k++;
                    }
                    DtThreat.ItemsSource = listDatabases;
                    DtThreat.Items.Refresh();
                }
                else
                {
                    Page++;
                    TbNumberList.Text = $"с {(Page - 1) * NumberOfAttributes} до {mainDatabases.Last().IdenexYBI}";
                    ThreatDatabase[] listDatabases = new ThreatDatabase[mainDatabases.Length % NumberOfAttributes];
                    int k = 0;
                    for (int i = (Page - 1) * NumberOfAttributes; i < Convert.ToInt32(mainDatabases.Last().IdenexYBI); i++)
                    {
                        listDatabases[k] = mainDatabases[i];
                        k++;
                    }
                    DtThreat.ItemsSource = listDatabases;
                    DtThreat.Items.Refresh();
                }
            }
            else
            {
                if ((Page) * NumberOfAttributes > Convert.ToInt32(mainDatabases.Last().IdenexYBI))
                {
                    return;
                }
                if ((Page + 1) * NumberOfAttributes < Convert.ToInt32(mainDatabases.Last().IdenexYBI))
                {
                    Page++;
                    TbNumberList.Text = $"с {(Page - 1) * NumberOfAttributes} до {NumberOfAttributes * Page}";
                    threatDatabase_Id_Names = new ThreatDatabase_id_Name[NumberOfAttributes];
                    int k = 0;
                    for (int i = (Page - 1) * NumberOfAttributes; i < NumberOfAttributes * Page; i++)
                    {
                        threatDatabase_Id_Names[k] = new ThreatDatabase_id_Name { IdenexYBI = mainDatabases[i].IdenexYBI, NameYBI = mainDatabases[i].NameYBI };
                        k++;
                    }
                    DtThreat.ItemsSource = threatDatabase_Id_Names;
                    DtThreat.Items.Refresh();
                }
                else
                {
                    Page++;
                    TbNumberList.Text = $"с {(Page - 1) * NumberOfAttributes} до {mainDatabases.Last().IdenexYBI}";
                    threatDatabase_Id_Names = new ThreatDatabase_id_Name[mainDatabases.Length % NumberOfAttributes];
                    int k = 0;
                    for (int i = (Page - 1) * NumberOfAttributes; i < Convert.ToInt32(mainDatabases.Last().IdenexYBI); i++)
                    {
                        threatDatabase_Id_Names[k] = new ThreatDatabase_id_Name { IdenexYBI = mainDatabases[i].IdenexYBI, NameYBI = mainDatabases[i].NameYBI };
                        k++;
                    }
                    DtThreat.ItemsSource = threatDatabase_Id_Names;
                    DtThreat.Items.Refresh();
                }
            }
        }

        private void BtnStartList_Click(object sender, RoutedEventArgs e)
        {
            if (Rduction == false)
            {
                if (Page != 1)
                {
                    Page = 1;
                    TbNumberList.Text = $"с {Page} до {NumberOfAttributes * Page}";
                    ThreatDatabase[] listDatabases = new ThreatDatabase[NumberOfAttributes];
                    int k = 0;
                    for (int i = (Page - 1) * NumberOfAttributes; i < NumberOfAttributes * Page; i++)
                    {
                        listDatabases[k] = mainDatabases[i];
                        k++;
                    }
                    DtThreat.ItemsSource = listDatabases;
                    DtThreat.Items.Refresh();
                }
            }
            else
            {
                Page = 1;
                TbNumberList.Text = $"с {Page} до {NumberOfAttributes * Page}";
                threatDatabase_Id_Names = new ThreatDatabase_id_Name[NumberOfAttributes];
                int k = 0;
                for (int i = (Page - 1) * NumberOfAttributes; i < NumberOfAttributes * Page; i++)
                {
                    threatDatabase_Id_Names[k] = threatDatabase_Id_Names[k] = new ThreatDatabase_id_Name { IdenexYBI = mainDatabases[i].IdenexYBI, NameYBI = mainDatabases[i].NameYBI };
                    k++;
                }
                DtThreat.ItemsSource = threatDatabase_Id_Names;
                DtThreat.Items.Refresh();
            }
        }

        private void BtnEndList_Click(object sender, RoutedEventArgs e)
        {
            if (Rduction == false)
            {
                if (Page != Convert.ToInt32(Math.Ceiling(Convert.ToDouble(mainDatabases.Last().IdenexYBI) / NumberOfAttributes)))
                {
                    Page = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(mainDatabases.Last().IdenexYBI) / NumberOfAttributes));
                    TbNumberList.Text = $"с {(Page - 1) * NumberOfAttributes} до {mainDatabases.Last().IdenexYBI}";
                    ThreatDatabase[] listDatabases = new ThreatDatabase[mainDatabases.Length % NumberOfAttributes];
                    int k = 0;
                    for (int i = (Page - 1) * NumberOfAttributes; i < Convert.ToInt32(mainDatabases.Last().IdenexYBI); i++)
                    {
                        listDatabases[k] = mainDatabases[i];
                        k++;
                    }
                    DtThreat.ItemsSource = listDatabases;
                    DtThreat.Items.Refresh();
                }
            }
            else
            {
                if (Page != Convert.ToInt32(Math.Ceiling(Convert.ToDouble(mainDatabases.Last().IdenexYBI) / NumberOfAttributes)))
                {
                    Page = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(mainDatabases.Last().IdenexYBI) / NumberOfAttributes));
                    TbNumberList.Text = $"с {(Page - 1) * NumberOfAttributes} до {mainDatabases.Last().IdenexYBI}";
                    threatDatabase_Id_Names = new ThreatDatabase_id_Name[mainDatabases.Length % NumberOfAttributes];
                    int k = 0;
                    for (int i = (Page - 1) * NumberOfAttributes; i < Convert.ToInt32(mainDatabases.Last().IdenexYBI); i++)
                    {
                        threatDatabase_Id_Names[k] = new ThreatDatabase_id_Name { IdenexYBI = mainDatabases[i].IdenexYBI, NameYBI = mainDatabases[i].NameYBI };
                        k++;
                    }
                    DtThreat.ItemsSource = threatDatabase_Id_Names;
                    DtThreat.Items.Refresh();
                }
            }
        }

        private void DtThreat_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (CheckUpdate != true)
            {
                if (Rduction == false)
                {
                    try
                    {
                        if (sender != null)
                        {
                            DataGrid grid = sender as DataGrid;
                            if (grid != null && grid.SelectedItems != null && grid.SelectedItems.Count == 1)
                            {
                                DataGridRow dgr = grid.ItemContainerGenerator.ContainerFromItem(grid.SelectedItem) as DataGridRow;
                                ThreatDatabase threatDatabase = (ThreatDatabase)dgr.Item;

                                MessageBox.Show("Id угрозы: " + threatDatabase.IdenexYBI + "\n\nИмя угрозы:\n" + threatDatabase.NameYBI + "\n\nОписание :\n" + threatDatabase.Description + "\n\nИсточник угрозы :\n" + threatDatabase.SourceThreats + "\n\nОбъект воздействия угрозы :\n" + threatDatabase.ObjectAction + "\n\nНарушение конфиденциальности : " + threatDatabase.BreachPrivacy + "\n\nНарушение целостности : " + threatDatabase.ViolationIntegrity + "\n\nНарушение доступности : " + threatDatabase.AccessViolation, "Информация о строке");
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message.ToString());
                    }
                }
                else
                {
                    try
                    {
                        if (sender != null)
                        {
                            DataGrid grid = sender as DataGrid;
                            if (grid != null && grid.SelectedItems != null && grid.SelectedItems.Count == 1)
                            {
                                DataGridRow dgr = grid.ItemContainerGenerator.ContainerFromItem(grid.SelectedItem) as DataGridRow;
                                ThreatDatabase_id_Name threatDatabase_Id_Name = (ThreatDatabase_id_Name)dgr.Item;

                                MessageBox.Show("Id угрозы: " + threatDatabase_Id_Name.IdenexYBI + "\n\nИмя угрозы:\n" + threatDatabase_Id_Name.NameYBI, "Информация о строке");
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message.ToString());
                    }
                }
            }
            else
            {
                try
                {
                    if (sender != null)
                    {
                        DataGrid grid = sender as DataGrid;
                        if (grid != null && grid.SelectedItems != null && grid.SelectedItems.Count == 1)
                        {
                            DataGridRow dgr = grid.ItemContainerGenerator.ContainerFromItem(grid.SelectedItem) as DataGridRow;
                            DatabaseOldNew databaseOldNew = (DatabaseOldNew)dgr.Item;

                            MessageBox.Show("Id угрозы: " + databaseOldNew.IdenexYBI + "\n\nИмя угрозы:\n" + databaseOldNew.OldAttribute + "\n\nСтарый атрибут :\n" + databaseOldNew.NewAttribute + "\n\nНовый атрибут:\n", "Информация о строке");
                        }
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message.ToString());
                }
            }
        }

        private void BtnRductionGrid_Click(object sender, RoutedEventArgs e)
        {
            ControlColourText();
            if (Rduction == true)
            {
                threatDatabase_Id_Names = new ThreatDatabase_id_Name[NumberOfAttributes];
                int k = 0;
                for (int i = (Page - 1) * NumberOfAttributes; i < NumberOfAttributes * Page; i++)
                {
                    threatDatabase_Id_Names[k] = new ThreatDatabase_id_Name { IdenexYBI = mainDatabases[i].IdenexYBI, NameYBI = mainDatabases[i].NameYBI };
                    k++;
                }
                DtThreat.ItemsSource = threatDatabase_Id_Names;
            }
            else
            {
                ThreatDatabase[] listDatabases = new ThreatDatabase[NumberOfAttributes];
                int k = 0;
                for (int i = (Page - 1) * NumberOfAttributes; i < NumberOfAttributes * Page; i++)
                {
                    listDatabases[k] = mainDatabases[i];
                    k++;
                }
                DtThreat.ItemsSource = listDatabases;
                DtThreat.Items.Refresh();
            }
        }
        private void BtnUpdate_Click(object sender, RoutedEventArgs e)
        {
            //Около конпки добавить дату последнего обнавления
            int oldNewCount = 0;
            BtnClose.Visibility = Visibility.Visible;
            CheckUpdate = true;
            try
            {
                if (File.Exists(Path + FaileName))
                {
                   // client.DownloadFile(uri.OriginalString, "New_" + FaileName);
                    ThreatDatabase[] newDatabases = ExcelParser.ReadExcel(Path + "New_" + FaileName);
                    databaseOldNew = new List<DatabaseOldNew>();
                    int maxLenght = (newDatabases.Length > mainDatabases.Length) ? newDatabases.Length : mainDatabases.Length;
                    bool newArrMore = (maxLenght == newDatabases.Length) ? true : false;

                    for (int i = 0; i < maxLenght; i++)
                    {
                        try
                        {
                            if (newDatabases[i].IdenexYBI != mainDatabases[i].IdenexYBI)
                            {
                                databaseOldNew.Add(new DatabaseOldNew { IdenexYBI = newDatabases[i].IdenexYBI, NewAttribute = newDatabases[i].IdenexYBI, OldAttribute = mainDatabases[i].IdenexYBI });
                                oldNewCount++;
                            }

                            if (newDatabases[i].NameYBI != mainDatabases[i].NameYBI)
                            {
                                databaseOldNew.Add(new DatabaseOldNew { IdenexYBI = newDatabases[i].IdenexYBI, NewAttribute = newDatabases[i].NameYBI, OldAttribute = mainDatabases[i].NameYBI });
                                oldNewCount++;
                            }

                            if (newDatabases[i].Description != mainDatabases[i].Description)
                            {
                                databaseOldNew.Add(new DatabaseOldNew { IdenexYBI = newDatabases[i].IdenexYBI, NewAttribute = newDatabases[i].Description, OldAttribute = mainDatabases[i].Description });
                                oldNewCount++;
                            }

                            if (newDatabases[i].SourceThreats != mainDatabases[i].SourceThreats)
                            {
                                databaseOldNew.Add(new DatabaseOldNew { IdenexYBI = newDatabases[i].IdenexYBI, NewAttribute = newDatabases[i].SourceThreats, OldAttribute = mainDatabases[i].SourceThreats });
                                oldNewCount++;
                            }

                            if (newDatabases[i].ObjectAction != mainDatabases[i].ObjectAction)
                            {
                                databaseOldNew.Add(new DatabaseOldNew { IdenexYBI = newDatabases[i].IdenexYBI, NewAttribute = newDatabases[i].ObjectAction, OldAttribute = mainDatabases[i].ObjectAction });
                                oldNewCount++;
                            }

                            if (newDatabases[i].BreachPrivacy != mainDatabases[i].BreachPrivacy)
                            {
                                databaseOldNew.Add(new DatabaseOldNew { IdenexYBI = newDatabases[i].IdenexYBI, NewAttribute = newDatabases[i].BreachPrivacy, OldAttribute = mainDatabases[i].BreachPrivacy });
                                oldNewCount++;
                            }

                            if (newDatabases[i].ViolationIntegrity != mainDatabases[i].ViolationIntegrity)
                            {
                                databaseOldNew.Add(new DatabaseOldNew { IdenexYBI = newDatabases[i].IdenexYBI, NewAttribute = newDatabases[i].ViolationIntegrity, OldAttribute = mainDatabases[i].ViolationIntegrity });
                                oldNewCount++;
                            }

                            if (newDatabases[i].AccessViolation != mainDatabases[i].AccessViolation)
                            {
                                databaseOldNew.Add(new DatabaseOldNew { IdenexYBI = newDatabases[i].IdenexYBI, NewAttribute = newDatabases[i].AccessViolation, OldAttribute = mainDatabases[i].AccessViolation });
                                oldNewCount++;
                            }
                        }
                        catch (Exception)
                        {
                            if (newArrMore == true)
                            {
                                databaseOldNew.Add(new DatabaseOldNew { IdenexYBI = newDatabases[i].IdenexYBI, NewAttribute = newDatabases[i].IdenexYBI, OldAttribute = "Пусто" });
                                databaseOldNew.Add(new DatabaseOldNew { IdenexYBI = newDatabases[i].IdenexYBI, NewAttribute = newDatabases[i].NameYBI, OldAttribute = "Пусто" });
                                databaseOldNew.Add(new DatabaseOldNew { IdenexYBI = newDatabases[i].IdenexYBI, NewAttribute = newDatabases[i].Description, OldAttribute = "Пусто" });
                                databaseOldNew.Add(new DatabaseOldNew { IdenexYBI = newDatabases[i].IdenexYBI, NewAttribute = newDatabases[i].SourceThreats, OldAttribute = "Пусто" });
                                databaseOldNew.Add(new DatabaseOldNew { IdenexYBI = newDatabases[i].IdenexYBI, NewAttribute = newDatabases[i].ObjectAction, OldAttribute = "Пусто" });
                                databaseOldNew.Add(new DatabaseOldNew { IdenexYBI = newDatabases[i].IdenexYBI, NewAttribute = newDatabases[i].BreachPrivacy, OldAttribute = "Пусто" });
                                databaseOldNew.Add(new DatabaseOldNew { IdenexYBI = newDatabases[i].IdenexYBI, NewAttribute = newDatabases[i].ViolationIntegrity, OldAttribute = "Пусто" });
                                databaseOldNew.Add(new DatabaseOldNew { IdenexYBI = newDatabases[i].IdenexYBI, NewAttribute = newDatabases[i].AccessViolation, OldAttribute = "Пусто" });
                                oldNewCount += 8;
                            }
                            else
                            {
                                databaseOldNew.Add(new DatabaseOldNew { IdenexYBI = newDatabases[i].IdenexYBI, NewAttribute = "Пусто", OldAttribute = mainDatabases[i].IdenexYBI });
                                databaseOldNew.Add(new DatabaseOldNew { IdenexYBI = newDatabases[i].IdenexYBI, NewAttribute = "Пусто", OldAttribute = mainDatabases[i].NameYBI });
                                databaseOldNew.Add(new DatabaseOldNew { IdenexYBI = newDatabases[i].IdenexYBI, NewAttribute = "Пусто", OldAttribute = mainDatabases[i].Description });
                                databaseOldNew.Add(new DatabaseOldNew { IdenexYBI = newDatabases[i].IdenexYBI, NewAttribute = "Пусто", OldAttribute = mainDatabases[i].SourceThreats });
                                databaseOldNew.Add(new DatabaseOldNew { IdenexYBI = newDatabases[i].IdenexYBI, NewAttribute = "Пусто", OldAttribute = mainDatabases[i].ObjectAction });
                                databaseOldNew.Add(new DatabaseOldNew { IdenexYBI = newDatabases[i].IdenexYBI, NewAttribute = "Пусто", OldAttribute = mainDatabases[i].BreachPrivacy });
                                databaseOldNew.Add(new DatabaseOldNew { IdenexYBI = newDatabases[i].IdenexYBI, NewAttribute = "Пусто", OldAttribute = mainDatabases[i].ViolationIntegrity });
                                databaseOldNew.Add(new DatabaseOldNew { IdenexYBI = newDatabases[i].IdenexYBI, NewAttribute = "Пусто", OldAttribute = mainDatabases[i].AccessViolation });
                                oldNewCount += 8;
                            }
                        }
                    }
                    if (oldNewCount > 0)
                    {
                        MessageBox.Show("Успешно", "Проверка обновления");
                        MessageBox.Show($"Общее количестов обновлений: {oldNewCount}", "Количество обновлений");

                        DtThreat.ItemsSource = databaseOldNew;
                    }
                    else
                    {
                        MessageBox.Show("Нечего обновлять", "Обновление");
                    }
                }
                else
                {
                    MessageBoxResult result = System.Windows.MessageBox.Show("Файл не найден, скачать с интернета?", "Файл не найден", MessageBoxButton.YesNo, MessageBoxImage.Question);

                    if (result == MessageBoxResult.Yes)
                    {
                        if (!string.IsNullOrEmpty(uri.OriginalString))
                        {
                            client.DownloadFile(uri.OriginalString, FaileName);
                            BtnUpdate_Click(sender, e);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Возникла ошибка");
            }
        }
        private void BtnClose_Click(object sender, RoutedEventArgs e)
        {
            CheckUpdate = false;
            MessageBoxResult result = System.Windows.MessageBox.Show("Заменить текушию таблицу на скачанную?", "Замена", MessageBoxButton.YesNo, MessageBoxImage.Question);

            if (result == MessageBoxResult.Yes)
            {
                if (!string.IsNullOrEmpty(uri.OriginalString))
                {
                    if (File.Exists(Path + "New_" + FaileName))
                    {
                        File.Delete(Path + "New_" + FaileName);
                    }
                    if (File.Exists(Path + FaileName))
                    {
                        File.Delete(Path + FaileName);
                    }
                    client.DownloadFile(uri.OriginalString, FaileName);
                }
            }
            if (Rduction == true)
            {
                threatDatabase_Id_Names = new ThreatDatabase_id_Name[NumberOfAttributes];
                int k = 0;
                for (int i = (Page - 1) * NumberOfAttributes; i < NumberOfAttributes * Page; i++)
                {
                    threatDatabase_Id_Names[k] = new ThreatDatabase_id_Name { IdenexYBI = mainDatabases[i].IdenexYBI, NameYBI = mainDatabases[i].NameYBI };
                    k++;
                }
                DtThreat.ItemsSource = threatDatabase_Id_Names;
            }
            else
            {
                ThreatDatabase[] listDatabases = new ThreatDatabase[NumberOfAttributes];
                int k = 0;
                for (int i = (Page - 1) * NumberOfAttributes; i < NumberOfAttributes * Page; i++)
                {
                    listDatabases[k] = mainDatabases[i];
                    k++;
                }
                DtThreat.ItemsSource = listDatabases;
                DtThreat.Items.Refresh();
            }
            BtnClose.Visibility = Visibility.Hidden;
        }
        public void ControlColourText()
        {
            Rduction = !Rduction;
            if (Rduction == true)
            {
                TbOn.Foreground = Brushes.Green;
                TbOff.Foreground = Brushes.Black;
            }
            else
            {
                TbOn.Foreground = Brushes.Black;
                TbOff.Foreground = Brushes.Red;
            }
        }

    }
}
