﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laba_2_NowYouSeeSharp_2
{
   public class ThreatDatabase
    {
        public string IdenexYBI { get; set; }// Индификатор
        public string NameYBI { get; set; }// Наименование
        public string Description { get; set; }// Описание
        public string SourceThreats { get; set; }// Источник угрощы
        public string ObjectAction { get; set; }// Обьект воздействия
        public string BreachPrivacy { get; set; }// Нарушение конфиденциальности
        public string ViolationIntegrity { get; set; }// Нарушение целостности
        public string AccessViolation { get; set; }// Нарушение доступности

        public override string ToString()
        {
            return $"{IdenexYBI} {NameYBI} {Description} {SourceThreats} {ObjectAction} {BreachPrivacy} {ViolationIntegrity} {AccessViolation}";
        }
    }
}
