﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using OfficeOpenXml;
using System.Windows;

namespace Laba_2_NowYouSeeSharp_2
{
   public class ExcelParser
    {
        public static ThreatDatabase[] ReadExcel(string FileName)
        {
            try
            {
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                var fi = new FileInfo(FileName);
                using (var package = new ExcelPackage(fi))
                {
                    using (ExcelWorksheet workSheet = package.Workbook.Worksheets[0])
                    {
                        ThreatDatabase[] threatDatabases = new ThreatDatabase[workSheet.Dimension.End.Row - 2];
                        int countThreat = 0;
                        var list1 = workSheet.Cells[workSheet.Dimension.Start.Row + 2, 1, workSheet.Dimension.End.Row, 8].ToList();
                        for (int i = 0; i < list1.Count; i += 8)
                        {
                            threatDatabases[countThreat] = new ThreatDatabase();
                            threatDatabases[countThreat].IdenexYBI = list1[i].Text;
                            threatDatabases[countThreat].NameYBI = list1[i + 1].Text;
                            threatDatabases[countThreat].Description = list1[i + 2].Text;
                            threatDatabases[countThreat].SourceThreats = list1[i + 3].Text;
                            threatDatabases[countThreat].ObjectAction = list1[i + 4].Text;

                            if (list1[i + 5].Text == "1")
                            {
                                threatDatabases[countThreat].BreachPrivacy = "да";
                            }
                            else
                            {
                                threatDatabases[countThreat].BreachPrivacy = "нет";
                            }

                            if (list1[i + 6].Text == "1")
                            {
                                threatDatabases[countThreat].ViolationIntegrity = "да";
                            }
                            else
                            {
                                threatDatabases[countThreat].ViolationIntegrity = "нет";
                            }

                            if (list1[i + 7].Text == "1")
                            {
                                threatDatabases[countThreat].AccessViolation = "да";
                            }
                            else
                            {
                                threatDatabases[countThreat].AccessViolation = "нет";
                            }
                            countThreat++;
                        }
                        return threatDatabases;
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
            return null;
        }
    }
}
